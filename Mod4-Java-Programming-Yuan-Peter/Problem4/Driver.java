public class Driver {
    public static void main(String[] args) {
        // desktop computer
        Desktop myHomePC = new Desktop(true, "ATX", 16, "AMD", "NVIDIA", true, 512, "Linux", 8.7, 1000, 300, 2, "N/A");
        System.out.println(myHomePC.toString());

        // laptop
        Laptop macbook = new Laptop(14, 60, false, 8, "Intel", "Intel", true, 256, "MacOS", 2.7, 1200, 30, 3, "Apple");
        System.out.println(macbook.toString());

        // cellphone
        Phone iphone = new Phone(5.7, 17.8, true, 2, "Apple", "Apple", true, 128, "iOS", 0.8, 600, 5, 3, "Apple");
        System.out.println(iphone.toString());

        // camera
        Camera cannon = new Camera(11.4, 3, true, 3, 430, 50, 5, "Cannon");
        System.out.println(cannon.toString());

        // mechanical keyboard
        Keyboard leopold = new Keyboard("TKL", "Tactile", "Cherry", "GMK White On Black", 4.43, 120, 10, 2, "Leopold");
        System.out.println(leopold.toString());
    }
}