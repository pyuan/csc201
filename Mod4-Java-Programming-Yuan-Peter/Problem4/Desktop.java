
public class Desktop extends Computer {
    private boolean isTemperedGlass;
    private String caseSize;

    public Desktop() {
    }

    public Desktop(boolean isTemperedGlass, String caseSize,
    int ram, String cpu, String gpu, boolean storageSsd, int storageSize, String operatingSystem,
    double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.isTemperedGlass = isTemperedGlass;
        this.caseSize = caseSize;

        setRam(ram);
        setCpu(cpu);
        setGpu(gpu);
        setStorageSsd(storageSsd);
        setStorageSize(storageSize);
        setOperatingSystem(operatingSystem);

        setWeight(weight);
        setCost(cost);
        setPowerUsage(powerUsage);
        setWarrantyInYears(warrantyInYears);
        setManufacturer(manufacturer);
    }

    public String toString() {
        return "\nDesktop Computer" +
        super.toString() +
        getSpecs() +
        "\nTempered Glass: " + isTemperedGlass + 
        "\nCase Size: " + caseSize;
    }
}