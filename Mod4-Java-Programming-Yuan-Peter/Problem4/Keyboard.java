public class Keyboard extends Electronics {
    private String formFactor; 
    private String switchType;
    private String keycapType;
    private String keycapName;

    public Keyboard() {
    }

    public Keyboard(String formFactor, String switchType, String keycapType, String keycapName,
    double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.formFactor = formFactor;
        this.switchType = switchType;
        this.keycapType = keycapType;
        this.keycapName = keycapName;

        setWeight(weight);
        setCost(cost);
        setPowerUsage(powerUsage);
        setWarrantyInYears(warrantyInYears);
        setManufacturer(manufacturer);
    }

    public String toString() {
        return "\nMechanical Keyboard" + 
        super.toString() +
        "\nForm Factor: " + formFactor +
        "\nSwitch Type: " + switchType +
        "\nKeycap Type: " + keycapType +
        "\nKeycap Name: " + keycapName;
    }
}

