public class Electronics {
    private double weight;
    private double cost;
    private double powerUsage;
    private int warrantyInYears;
    private String manufacturer;

    public Electronics() {
    }

    public Electronics(double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.weight = weight;
        this.cost = cost;
        this.powerUsage = powerUsage;
        this.warrantyInYears = warrantyInYears;
        this.manufacturer = manufacturer;
    }

    public String toString() {
        return 
            "\nWeight: " + weight + "lbs" +
            "\nCost: " + cost +  " dollars" +
            "\nPower Usage: " + powerUsage +
            "\nWarranty In Years: " + warrantyInYears +
            "\nManufacturer: " + manufacturer;
    }

    // set methods

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setPowerUsage(double powerUsage) {
        this.powerUsage = powerUsage;
    }

    public void setWarrantyInYears(int warrantyInYears) {
        this.warrantyInYears = warrantyInYears;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    // get methods

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public double getPowerUsage() {
        return powerUsage;
    }

    public int getWarrantyInYears() {
        return warrantyInYears;
    }

    public String getManufacturer() {
        return manufacturer;
    }
}