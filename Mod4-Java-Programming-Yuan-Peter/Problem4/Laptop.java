public class Laptop extends Computer {
    private double screenSize;    
    private double batterySize;
    private boolean hasTrackPoint;

    public Laptop() {
    }

    public Laptop(double screenSize, double batterySize, boolean hasTrackPoint,
    int ram, String cpu, String gpu, boolean storageSsd, int storageSize, String operatingSystem,
    double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.screenSize = screenSize;
        this.batterySize = batterySize;
        this.hasTrackPoint = hasTrackPoint;

        setRam(ram);
        setCpu(cpu);
        setGpu(gpu);
        setStorageSsd(storageSsd);
        setStorageSize(storageSize);
        setOperatingSystem(operatingSystem);

        setWeight(weight);
        setCost(cost);
        setPowerUsage(powerUsage);
        setWarrantyInYears(warrantyInYears);
        setManufacturer(manufacturer);

    }

    public String toString() {
        return "\nLaptop Computer" + 
        super.toString() +
        getSpecs() +
        "\nScreen Size: " + screenSize +
        "\nBattery Size: " + batterySize +
        "\nHas Track Point: " + hasTrackPoint;
    }
}