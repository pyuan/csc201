public class Phone extends Computer {
    private double screenSize;    
    private double batterySize;
    private boolean hasFingerPrintScanner;

    public Phone() {
    }

    public Phone(double screenSize, double batterySize, boolean hasFingerPrintScanner,
    int ram, String cpu, String gpu, boolean storageSsd, int storageSize, String operatingSystem,
    double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.screenSize = screenSize;
        this.batterySize = batterySize;
        this.hasFingerPrintScanner = hasFingerPrintScanner;

        setRam(ram);
        setCpu(cpu);
        setGpu(gpu);
        setStorageSsd(storageSsd);
        setStorageSize(storageSize);
        setOperatingSystem(operatingSystem);

        setWeight(weight);
        setCost(cost);
        setPowerUsage(powerUsage);
        setWarrantyInYears(warrantyInYears);
        setManufacturer(manufacturer);

    }

    public String toString() {
        return "\nPhone" + 
        super.toString() +
        getSpecs() +
        "\nScreen Size: " + screenSize +
        "\nBattery Size: " + batterySize + 
        "\nHas Fingerprint Scanner: " + hasFingerPrintScanner;
    }
}