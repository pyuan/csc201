public class Computer extends Electronics {
    private int ram;
    private String cpu;
    private String gpu;
    private boolean storageSsd;
    private int storageSize;
    private String operatingSystem;

    // set methods

    public void setRam(int ram) {
        this.ram = ram;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public void setStorageSsd(boolean storageSsd) {
        this.storageSsd = storageSsd;
    }

    public void setStorageSize(int storageSize) {
        this.storageSize = storageSize;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }


    public String getSpecs() {
        return 
        "\nSpecifications" + 
        "\nCPU: " + cpu +
        "\nRAM: " + ram + "gb" +
        "\nSSD: " + storageSsd + 
        "\nDisk Space: " + storageSize + "gb" +
        "\nGPU: " + gpu + 
        "\nOperating System: " + operatingSystem;
    }

}