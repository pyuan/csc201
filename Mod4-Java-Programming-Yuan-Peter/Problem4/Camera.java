public class Camera extends Electronics {
    private double lensSize; 
    private int numberOfZoomLevels;
    private boolean hasFlash;

    public Camera() {
    }

    public Camera(double lensSize, int numberOfZoomLevels, boolean hasFlash,
    double weight, double cost, double powerUsage, int warrantyInYears, String manufacturer) {
        this.lensSize = lensSize;
        this.numberOfZoomLevels = numberOfZoomLevels;
        this.hasFlash = hasFlash;

        setWeight(weight);
        setCost(cost);
        setPowerUsage(powerUsage);
        setWarrantyInYears(warrantyInYears);
        setManufacturer(manufacturer);
    }

    public String toString() {
        return "\nCamera" + 
        super.toString() + 
        "\nLens Size: " + lensSize +
        "\nNumber of Zoom Levels: " + numberOfZoomLevels +
        "\nHas Flash: " + hasFlash;

    }
}
