public class Book extends ReadingMaterials {
    private boolean eBookAvailable, isFiction;

    Book() {
    }

    Book(boolean eBookAvailable, boolean isFiction, String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.eBookAvailable = eBookAvailable;
        this.isFiction = isFiction;
        setTitle(title);
        setAuthor(author);
        setDatePublished(datePublished);
        setNumberOfPages(numberOfPages);
        setIsHardCover(isHardCover);
    }
    
    public String toString() {
        return "Book\n" + super.toString() + "\nE-Book Available: " + eBookAvailable + "\nFiction: " + isFiction;
    }

    public void setEBookAvailable(boolean eBookAvailable) {
        this.eBookAvailable = eBookAvailable;
    }

    public void setIsFiction(boolean isFiction) {
        this.isFiction = isFiction;
    }
    
    public boolean getEBookAvailable() {
        return eBookAvailable;
    }

    public boolean getIsFiction() {
        return isFiction;
    }
}