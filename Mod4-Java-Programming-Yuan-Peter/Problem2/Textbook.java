public class Textbook extends ReadingMaterials {
    private String schoolLevel;   
    private double costPerUnit;
    private int unitsOrdered;

    public Textbook() {
    }

    public Textbook(String schoolLevel, double costPerUnit, int unitsOrdered, String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.schoolLevel = schoolLevel;
        this.costPerUnit = costPerUnit;
        this.unitsOrdered = unitsOrdered;
        setTitle(title);
        setAuthor(author);
        setDatePublished(datePublished);
        setNumberOfPages(numberOfPages);
        setIsHardCover(isHardCover);
    }

    public String toString() {
        return "TextBook\n" + super.toString() + "\nSchool Level: " + schoolLevel + "\nCost Per Unit: " + costPerUnit + "\nUnits Ordered: " + unitsOrdered
        + "\nTotal Cost: " + calculateTotalCost();
    }

    public double calculateTotalCost() {
        return costPerUnit * unitsOrdered;
    }

    public void addOrder() {
        unitsOrdered++;
    }

    public void minusOrder() {
        unitsOrdered--;
    }

    public void setCostPerUnit(float costPerUnit) {
        this.costPerUnit = costPerUnit;
    }
}