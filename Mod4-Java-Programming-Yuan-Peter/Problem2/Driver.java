public class Driver {

    public static void main(String[] args) {
       // Book story1 = new Book(true, true, "Catching Fire", "Suzanne Collins", "2009", 392, false);
       Novel story1 = new Novel("Historical Fiction", "Joe", "John", true, "The battle for best J name", "The first J Duel",
       "Will D. Guy", "June 1 2020", 200, false);
        System.out.println("\n" + story1.toString());

        // Magazine
        Magazine nextGen = new Magazine(6, "Next Gen", "Quantum Computers Are Coming Soon", "August 10 2020", "Quantum Computing Are Closer Than You think",
        "Will Denis", "July 10 2020", 32, false);
        nextGen.addSubArticle("The Brain's True Nature");
        nextGen.addSubArticle("How The Moon Affects Tides");
        System.out.println("\n" + nextGen.toString());

        // Technical Journal
        TechnicalJournal roboticResearch = new TechnicalJournal("Robotics", "University of Cambridge", "July 12 2020",
        "How Robotics Will Save Mankind", "Jensen Vicks", "May 20 2020", 144, false);
        roboticResearch.addRelatedSubject("Prostetics");
        System.out.println("\n" + roboticResearch.toString());

        // Textbook
        Textbook biology101 = new Textbook("College", 59.95, 80, "Introduction to Biology", "NextPlus Education", "June 9 2011", 812, true);
        System.out.println("\n" + biology101.toString());
    }
}