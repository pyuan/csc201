import java.util.ArrayList;

public class Magazine extends ReadingMaterials {
    private int issueNumber;    
    private String magazineTitle;
    private String featuredArticle;
    private String nextIssueDate;
    private ArrayList<String> subArticles = new ArrayList<>();
    
    public Magazine() {
    }

    public Magazine(int issueNumber, String magazineTitle, String featuredArticle, String nextIssueDate,
    String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.issueNumber = issueNumber;
        this.magazineTitle = magazineTitle;
        this.featuredArticle = featuredArticle;
        this.nextIssueDate = nextIssueDate;
        setTitle(title);
        setAuthor(author);
        setDatePublished(datePublished);
        setNumberOfPages(numberOfPages);
        setIsHardCover(isHardCover);
    }

    public String toString() {
        return "Magainze\n" + super.toString() + "\nIssue Number: " + issueNumber + "\nMagazine Title: " + magazineTitle +
        "\nFeatured Article: " + featuredArticle + "\nSub Articles: " + subArticles.toString() + "\nNext Issue Date: " + nextIssueDate;
    }

    public void addSubArticle(String subArticle) {
        subArticles.add(subArticle);
    }

    public void removeSubArticle(String subArticle) {
        subArticles.remove(subArticle);
    }
}