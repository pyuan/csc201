public class Novel extends Book {
    private String genre, protagonist, antagonist; 
    private boolean isSeries;
    private String seriesName;

    public Novel() {
    }

    public Novel(String genre, String protagonist, String antagonist, boolean isSeries, String seriesName,
    String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.genre = genre;
        this.protagonist = protagonist;
        this.antagonist = antagonist;
        this.isSeries = isSeries;
        this.seriesName = seriesName;
        setTitle(title);
        setAuthor(author);
        setDatePublished(datePublished);
        setNumberOfPages(numberOfPages);
        setIsHardCover(isHardCover);
    }

    public String toString() {
        return "Novel\n" + super.toString() + "\nGenre: " + genre + "\nProtagonist: " + protagonist + "\nAntagonist: " + antagonist +
        "\nIs it a series: " + isSeries + "\nSeries name: " + seriesName;
    }
}