import java.util.ArrayList;

public class TechnicalJournal extends ReadingMaterials {
    private String fieldOfResearch; 
    private ArrayList<String> relatedSubjects = new ArrayList<>();
    private String lab;    
    private String lastRevised;

    TechnicalJournal() {
    }

    TechnicalJournal(String fieldOfResearch, String lab, String lastRevised,
    String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.fieldOfResearch = fieldOfResearch;
        this.lab = lab;
        this.lastRevised = lastRevised;
        setTitle(title);
        setAuthor(author);
        setDatePublished(datePublished);
        setNumberOfPages(numberOfPages);
        setIsHardCover(isHardCover);
    }

    public String toString() {
        return "Technical Journal\n" + super.toString() + "\nField of Research: " + fieldOfResearch +
        "\nLab Location: " + lab + "\nLast Revised: " + lastRevised + "\nRelated Subjects: " + relatedSubjects.toString();
    }

    public void addRelatedSubject(String relatedSubject) {
        relatedSubjects.add(relatedSubject);
    }

    public void removeRelatedSubject(String relatedSubject) {
        relatedSubjects.remove(relatedSubject);
    }

    public void setLastRevised(String lastRevised) {
        this.lastRevised = lastRevised;
    }
}