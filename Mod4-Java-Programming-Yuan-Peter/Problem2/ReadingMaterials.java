public class ReadingMaterials {
    private String title, author, datePublished;
    private int numberOfPages;
    private boolean isHardCover;

    ReadingMaterials() {
    }

    ReadingMaterials(String title, String author, String datePublished, int numberOfPages, boolean isHardCover) {
        this.title = title;
        this.author = author;
        this.datePublished = datePublished;
        this.numberOfPages = numberOfPages;
        this.isHardCover = isHardCover;
    }

    public String toString() {
        return "Title: " + title + "\nAuthor: " + author + "\nDate Published: " + datePublished +
        "\nNumber of Pages: " + numberOfPages + "\nHard Cover: " + isHardCover;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author; 
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished; 
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public void setIsHardCover(boolean isHardCover) {
        this.isHardCover = isHardCover;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public boolean getIsHardCover() {
        return isHardCover;
    }

}