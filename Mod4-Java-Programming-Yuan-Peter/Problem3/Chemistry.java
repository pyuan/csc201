public class Chemistry extends Science {
    private boolean labPPEProvided;
    private int numberOfLabProjects;

    public Chemistry() {
    }

    public Chemistry(boolean labPPEProvided, int numberOfLabProjects, int labHourRequirement, int researchPaperRequirement,
    String title, String code, String description, int credits, int courseLengthInWeeks) {
        this.labPPEProvided = labPPEProvided;
        this.numberOfLabProjects = numberOfLabProjects;
        setLabHourRequirement(labHourRequirement);
        setResearchPaperRequirement(researchPaperRequirement);
        setTitle(title);
        setCode(code);
        setDescription(description);
        setDepartment("Science");
        setCredits(credits); 
        setCourseLengthInWeeks(courseLengthInWeeks); 
    }

    public String toString() {
        return "\nChemistry" + super.toString() +
        "\nLab Hour Requirement: " + getLabHourRequirement() + 
        "\nResearch Paper Requirement: " + getResearchPaperRequirement() +
        "\nLab PPE Provided: " + labPPEProvided +
        "\nNumber of Lab Projects: " + numberOfLabProjects;

    }

}