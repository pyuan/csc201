public class AppliedMath extends Math {
    private int fieldProjects;
    
    public AppliedMath() {
    }

    public AppliedMath(int fieldProjects, int formulasMemorized, int proofsRequired, 
    String title, String code, String description, int credits, int courseLengthInWeeks) {
        this.fieldProjects = fieldProjects;
        setFormulasMemorized(formulasMemorized);
        setProofsRequired(proofsRequired);
        setTitle(title);
        setCode(code);
        setDescription(description);
        setDepartment("Mathematics");
        setCredits(credits); 
        setCourseLengthInWeeks(courseLengthInWeeks); 
    }

    public String toString() {
        return "\nApplied Math" + super.toString() + 
        "\nFormulas Memorized: " + getFormulasMemorized() +
        "\nProofs Required: " + getProofsRequired() +
        "\nField Projects Required: " + fieldProjects;
    }
}