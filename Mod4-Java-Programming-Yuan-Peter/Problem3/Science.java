public class Science extends Courses {
    private int labHourRequirement;
    private int researchPaperRequirement; 

    public Science() {
    }

    // set methods

    public void setLabHourRequirement(int labHourRequirement) {
        this.labHourRequirement = labHourRequirement;
    }

    public void setResearchPaperRequirement(int researchPaperRequirement) {
        this.researchPaperRequirement = researchPaperRequirement;
    }

    // get methods

    public int getLabHourRequirement() {
        return labHourRequirement;
    }

    public int getResearchPaperRequirement() {
        return researchPaperRequirement;
    }
}