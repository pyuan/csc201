import java.util.ArrayList;

public class Courses {
    private String title;
    private String code;
    private String description;
    private String department;
    private ArrayList<String> prerequisites = new ArrayList<>();
    private int credits;
    private int courseLengthInWeeks;

    public Courses() {
    }

    // Constructor
    
    public Courses(String title, String code, String description, String department, int credits, int courseLengthInWeeks) {
        this.title = title;
        this.code = code;
        this.description = description;
        this.department = department;
        this.credits = credits;
        this.courseLengthInWeeks = courseLengthInWeeks;
    }

    // Reused toString method

    public String toString() {
        return 
            "\nCourse Title: " + title +
            "\nCourse Code: " + code +
            "\nCourse Description: " + description +
            "\nPrerequisites: " + prerequisites.toString() +
            "\nCourse Department: " + department +
            "\nNumber Of Credits: " + credits +
            "\nCourse Length in Weeks: " + courseLengthInWeeks;
    }

    // Set Methods

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setCourseLengthInWeeks(int courseLengthInWeeks) {
        this.courseLengthInWeeks = courseLengthInWeeks;
    }
    
    public void setCredits(int credits) {
        this.credits = credits;
    }

    // Get Methods

    public String getTitle() {
        return title;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getDepartment() {
        return department;
    }

    public int getCredits() {
        return credits;
    }

    public void addPrerequisite(String prerequisite) {
        prerequisites.add(prerequisite);
    }
    

}