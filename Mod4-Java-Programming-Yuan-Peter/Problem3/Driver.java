public class Driver {
    
    public static void main(String[] args) {
        // Applied Math class
        AppliedMath mth361 = new AppliedMath(2, 6, 2, "Intro to Linear Algebra", "MTH361", "A math class about linear algebra", 4, 12);
        // Adds Prerequisite
        mth361.addPrerequisite("MTH291");
        mth361.addPrerequisite("MTH292");
        System.out.println(mth361.toString());

        // Pure Math class
        PureMath mth612 = new PureMath(5, 16, 8, "Functional Analysis", "MTH612", 
        "A branch of Mathematical Analaysis  the core of which is formed by the study of vector spaces endowed with some kind of limit-related structure (e.g. inner product, norm, topology, etc.) and the linear functions defined on these spaces and respecting these structures in a suitable sense",
        8, 20);
        mth612.addPrerequisite("MTH600");
        mth612.addPrerequisite("MTH601");
        System.out.println(mth612.toString());

        // Biology class
        Biology bio210 = new Biology(true, 40, 4, "Intro to Intermediate Biology", "BIO210", "Learn about more advanced biology topics", 4, 12);
        bio210.addPrerequisite("BIO100");
        System.out.println(bio210.toString());

        //Chemistry class
        Chemistry chm100 = new Chemistry(true, 2, 10, 1, "Introduction to Chemistry and Science", "CHM100", "A beginniner class about chemistry and science in general",
        2, 14);
        System.out.println(chm100.toString());

    }
}