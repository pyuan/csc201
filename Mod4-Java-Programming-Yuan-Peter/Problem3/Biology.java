public class Biology extends Science {
    private boolean hasDissection;

    public Biology() {
    }

    public Biology(boolean hasDissection, int labHourRequirement, int researchPaperRequirement,
    String title, String code, String description, int credits, int courseLengthInWeeks) {
        this.hasDissection = hasDissection;
        setLabHourRequirement(labHourRequirement);
        setResearchPaperRequirement(researchPaperRequirement);
        setTitle(title);
        setCode(code);
        setDescription(description);
        setDepartment("Science");
        setCredits(credits); 
        setCourseLengthInWeeks(courseLengthInWeeks); 
    }

    public String toString() {
        return "\nBiology" + super.toString() +
        "\nLab Hour Requirement: " + getLabHourRequirement() + 
        "\nResearch Paper Requirement: " + getResearchPaperRequirement() +
        "\nDissection Lab: " + hasDissection;
    }
}
