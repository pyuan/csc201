public class PureMath extends Math {
    private int researchProjects;

    public PureMath(int researchProjects, int formulasMemorized, int proofsRequired, 
    String title, String code, String description, int credits, int courseLengthInWeeks) {
        this.researchProjects = researchProjects;
        setFormulasMemorized(formulasMemorized);
        setProofsRequired(proofsRequired);
        setTitle(title);
        setCode(code);
        setDescription(description);
        setDepartment("Mathematics");
        setCredits(credits); 
        setCourseLengthInWeeks(courseLengthInWeeks); 
    }

    public String toString() {
        return "\nPure Math" + super.toString() + 
        "\nFormulas Memorized: " + getFormulasMemorized() +
        "\nProofs Required: " + getProofsRequired() +
        "\nResearch Projects Required: " + researchProjects;
    }

    
}