public class Math extends Courses {
    private int formulasMemorized;
    private int proofsRequired;


    public Math() {
    }

    public Math(int formulasMemorized, int proofsRequired, String title, String code, String description, int credits, int courseLengthInWeeks) {
        this.formulasMemorized = formulasMemorized;
        this.proofsRequired = proofsRequired;
        setTitle(title);
        setCode(code);
        setDescription(description);
        setDepartment("Mathematics");
        setCredits(credits); 
        setCourseLengthInWeeks(courseLengthInWeeks); 
    }

    // set methods

    public void setFormulasMemorized(int formulasMemorized) {
        this.formulasMemorized = formulasMemorized;
    }

    public void setProofsRequired(int proofsRequired) {
        this.proofsRequired = proofsRequired;
    }

    /// get methods

    public int getFormulasMemorized() {
        return formulasMemorized;
    }

    public int getProofsRequired() {
        return proofsRequired;
    }


}
