public class Driver {
    public static void main(String[] args) {
        // Basketball
        Basketball sport1 = new Basketball(99, 12, 900, "NBA", 100000, 30);
        System.out.println("Basketball Stats are:\n" + sport1.toString());

        // Soccer
        Soccer sport2 = new Soccer(3, 2, 4, "United States Soccer League System", 20000, 26);
        System.out.println("\nSoccer Stats are:\n" + sport2.toString());
    }
}