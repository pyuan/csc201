public class SportStats {
    private String leagueName;
    private int averageViewers;
    private int numberOfTeams;

    public SportStats() {
    }

    public SportStats(String leageName, int averageViewers, int numberOfTeams) {
        this.leagueName = leageName;
        this.averageViewers = averageViewers;
        this.numberOfTeams = numberOfTeams;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public int getAverageViewers() {
        return averageViewers;
    }

    public void setAverageViewers(int averageViewers) {
        this.averageViewers = averageViewers;
    }

    public int getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }
}