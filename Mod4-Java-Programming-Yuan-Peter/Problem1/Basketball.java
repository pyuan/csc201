public class Basketball extends SportStats {
    private int freeThrowRecord;
    private int numberOfOvertimes;
    private int numberOfThreePointers;

    public Basketball() {
    } 

    public Basketball
    (int freeThrowRecord, int numberOfOvertimes, int numberOfThreePointers, String leagueName, int averageViewers, int numberOfTeams) {
        this.freeThrowRecord = freeThrowRecord;
        this.numberOfOvertimes = numberOfOvertimes;
        this.numberOfThreePointers = numberOfThreePointers;
        setLeagueName(leagueName);
        setAverageViewers(averageViewers);
        setNumberOfTeams(numberOfTeams);
    }

    public String toString() {
        return "Free Throw Record: " + freeThrowRecord + "\nNumber of Overtimes: " + numberOfOvertimes
        + "\nNumber of three Pointers: " + numberOfOvertimes + "\nLeague Name: " + getLeagueName()
        + "\nAverage Viewers per Game: " + getAverageViewers() + "\nNumber of teams: " + getNumberOfTeams();
    }

    public int getFreeThrowRecord() {
        return freeThrowRecord;
    }

    public void setFreeThrowRecord(int freeThrowReocrd) {
        this.freeThrowRecord = freeThrowReocrd;
    }

    public void increaseFreeThrowRecord() {
        this.freeThrowRecord++;
    }

    public int getNumberOfOvertimes() {
        return numberOfOvertimes;
    }

    public void setNumberOfOvertimes(int numberOfOvertimes) {
        this.numberOfOvertimes = numberOfOvertimes;
    }

    public int getNumberOfThreePointers() {
        return numberOfThreePointers;
    }

    public void setNumberOfThreePointers(int numberOfThreePointers) {
        this.numberOfThreePointers = numberOfThreePointers;
    }

}