public class Soccer extends SportStats {
    private int averageGoalsPerGame;
    private int averageGoalSaves;
    private int seasonalInjuryCount;

    public Soccer() {
    }

    public Soccer(int averageGoalsPerGame, int averageGoalSaves, int seasonalInjuryCount, String leagueName, int averageViewers, int numberOfTeams) {
        this.averageGoalsPerGame = averageGoalsPerGame;
        this.averageGoalSaves = averageGoalSaves;
        this.seasonalInjuryCount = seasonalInjuryCount;
        setLeagueName(leagueName);
        setAverageViewers(averageViewers);
        setNumberOfTeams(numberOfTeams);
    }

    public String toString() {
        return "Average Goals Per Game: " + averageGoalsPerGame + "\nAverage Goal Saves: " + averageGoalSaves
        + "\nSeasonal Injury Count: " + seasonalInjuryCount + "\nLeague Name: " + getLeagueName() + "\nAverage Viewers Per Game"
        + getAverageViewers() + "\nNumber of teams: " + getNumberOfTeams();

    }

    public int getAverageGoalsPerGame() {
        return averageGoalsPerGame;
    }

    public void setAverageGoalsPerGame(int averageGoalsPerGame) {
        this.averageGoalsPerGame = averageGoalsPerGame;
    }

    public int getAverageGoalSaves() {
        return averageGoalSaves;
    }

    public void setAverageGoalSaves(int averageGoalSaves) {
        this.averageGoalSaves = averageGoalSaves;
    }

    public int getSeasonalInjuryCount() {
        return seasonalInjuryCount;
    }

    public void setSeasonalInjuryCount(int seasonalInjuryCount) {
        this.seasonalInjuryCount = seasonalInjuryCount;
    }

    public void incrementInjuryCount() {
        this.seasonalInjuryCount++;
    }
}