public class Bishop extends Piece {

    public Bishop() {
    }

    // constructor with arguments
    public Bishop(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♗");
        }
        else {
            setPicture("♝");
        }

    }
}