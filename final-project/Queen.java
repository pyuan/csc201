public class Queen extends Piece {

    public Queen() {
    }
    
    // constructor with arguments
    public Queen(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♕");
        }
        else {
            setPicture("♛");
        }

    }
}