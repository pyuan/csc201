public class Knight extends Piece {

    public Knight() {
    }

    // constructor with arguments
    public Knight(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♘");
        }
        else {
            setPicture("♞");
        }

    }
}