import java.util.Scanner;

public class Board { 
    private static Piece[][] board = new Piece[8][8];
    private static final Scanner input = new Scanner(System.in);

    public Board() {
        // initializes board

        for (int i = 0; i < 8; i++) {
            // set white pawns
            board[6][i] = new Pawn(6, i, true);
            // set black pawns
            board[1][i] = new Pawn(1, i, false);
        }

        // set white rook
        board[7][0] = new Rook(7, 0, true);
        board[7][7] = new Rook(7, 7, true);

        // set white knight
        board[7][1] = new Knight(7, 1, true);
        board[7][6] = new Knight(7, 6, true);

        // set white bishop
        board[7][2] = new Bishop(7, 2, true);
        board[7][5] = new Bishop(7, 5, true);

        // set white queen
        board[7][3] = new Queen(7, 3, true);

        // set white king
        board[7][4] = new King(7, 4, true);


        // set black rook
        board[0][0] = new Rook(0, 0, false);
        board[0][7] = new Rook(0, 7, false);

        // set black knight
        board[0][1] = new Knight(0, 1, false);
        board[0][6] = new Knight(0, 6, false);

        // set black bishop
        board[0][2] = new Bishop(0, 2, false);
        board[0][5] = new Bishop(0, 5, false);
        
        // set black queen
        board[0][3] = new Queen(0, 3, false);
        
        // set black king
        board[0][4] = new King(0, 4, false);

    }

    public void getBoard() {
        // prints board state
        // print top number coordinates
        System.out.println("  0 1 2 3 4 5 6 7");
        for (int i = 0; i < board.length; i++) {
            // print side number coordinates
            System.out.print(i + " ");
            for (int j = 0; j < board[i].length; j++) {
                // if index has no piece print 0
                if (board[i][j] == null) {
                    System.out.print("0 ");
                }
                // else print piece's picture
                else {
                    System.out.print(board[i][j].getPicture() + " ");
                }
            }
            System.out.println();
        }
    }

    public Piece getPiece(int x, int y) {
        // find Piece based on x y coordinates
        return board[x][y];
    }

    public void updatePriorPosition(Piece piece) {
        // removes where piece was
        board[piece.getX()][piece.getY()] = null;
    }

    public void updatePiecePosition(Piece piece) {
        // if piece is pawn and white and at board end then promote piece
        if (piece instanceof Pawn && piece.getIsWhite() == true && piece.getX() == 0) {
            pawnPromotion(piece);
        }
        // if piece is pawn and black and at board end then promote piece
        else if (piece instanceof Pawn && piece.getIsWhite() == false && piece.getX() == 7) {
            pawnPromotion(piece);
        }
        else {
            // normally move piece to position
            board[piece.getX()][piece.getY()] = piece;
        }

    }

    public static void pawnPromotion(Piece piece) {
        // pawn promotion
        // user prompted for which piece they want
        System.out.println("Your pawn can be promoted\n" + 
        "Enter 0 to promote to knight\n" +
        "Enter 1 to promote to bishop\n" +
        "Enter 2 to promote to rook\n" + 
        "Enter 3 to promote to queen");
        int choice = input.nextInt();
        switch (choice) {
            // knight promotion
            case 0: board[piece.getX()][piece.getY()] = new Knight(piece.getX(), piece.getY(), piece.getIsWhite()); break;
            // bishop promotion
            case 1: board[piece.getX()][piece.getY()] = new Bishop(piece.getX(), piece.getY(), piece.getIsWhite()); break;
            // rook promotion
            case 2: board[piece.getX()][piece.getY()] = new Rook(piece.getX(), piece.getY(), piece.getIsWhite()); break;
            // queen promotion
            case 3: board[piece.getX()][piece.getY()] = new Queen(piece.getX(), piece.getY(), piece.getIsWhite()); break;
        }

    }

}