public class Rectangle implements Comparable<Rectangle>  {
    private int side1, side2;

    public Rectangle() {
    }

    public Rectangle(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public int getArea() {
        return side1 * side2;
    }

    @Override
    public int compareTo(Rectangle r) {
        if (getArea() > r.getArea()) {
            return 1;
        }
        else if (getArea() < r.getArea()) {
            return -1;
        }
        else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Rectangle && getArea() == ((Rectangle) o).getArea();
    }


    