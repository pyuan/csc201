import java.util.Scanner;

public class PlayChess {
    private static int x, y, yMove, xMove;
    private static Piece piece;
    private static Board game = new Board();
    private static final Scanner input = new Scanner(System.in);


    public static void main(String[] args) throws InvalidTeamException {
        int continueSentinel = 1;
        // welcome player
        System.out.println("Welcome to Chess\n" + 
        // prompt user to hear rules or not
        "Enter 1 to hear rules or 0 to begin the game");
        int rulesChoice = input.nextInt();
        // rules
        if (rulesChoice == 1) {
            System.out.println("The goal of the game is checkmate the enemy king\n" +
            "To do this use your pieces to capture enemy pieces and check the king\n" +
            "A king is checkmates when he can no longer move to another square withoout also being checked\n" +
            "and no other piece can stop him from being checked.\n" +
            "This version relies on the players to decide when the king is checkmated, so pay attention!\n" +
            "For a full comprehensive ruleset visit\n" +
            "https://en.wikipedia.org/wiki/Rules_of_chess\n" +
            "The game will now begin\n");
        }
        // while game is continuing
        while (continueSentinel == 1) {
            // do while loop to ensure user enters valid coordinates
            boolean passException = true;
            do {
                try {
                    // prompt user for piece coordinates
                    // Player White's turn
                    userEnterPieceCoordinates(true);
                    passException = true;
                }
                catch (InvalidTeamException ex) {
                    // if exception then try loop is run again
                    passException = false;
                }
            } while (passException == false);
            // prompt user for new piece coordinates
            userEnterNewCoordinates();

            // same as above except for player Black
            passException = true;
            do {
                try {
                    userEnterPieceCoordinates(false);
                    passException = true;
                }
                catch (InvalidTeamException ex) {
                    passException = false;
                }
            } while (passException == false);
            userEnterNewCoordinates();
            
            System.out.println();
            // show board state
            game.getBoard();
            // prompt user to continue
            System.out.println("\nEnter 1 to play another round, enter 0 to quit");
            continueSentinel = input.nextInt();
            System.out.println();
        }
        // thank player for playing
        System.out.println("Thank you for playing Chess!");
    }

    public static void userEnterPieceCoordinates(boolean isWhite) throws InvalidTeamException {
        String color;
        boolean passException = true;
        System.out.println();
        // show board state
        game.getBoard();
        // determine whose turn it is
        if (isWhite == true) {
            color = "White";
        }
        else {
            color = "Black";     
        }
        // prompt player
        System.out.println("\nPlayer " + color + " make a move");
        System.out.println("Enter x coordinate and then y coordinate of piece");
        // do while loops ensures user chooses valid coordinates
        // will continually prompt user until they choose valid coordinates
        do {
            try {
                y = input.nextInt();
                x = input.nextInt();
                // find piece based on user entered coordinates
                piece = game.getPiece(x, y);
                // if user chose opposite team peace throw InvalidTeamException
                if (piece.getIsWhite() != isWhite) {
                    throw new InvalidTeamException(piece.getIsWhite());
                }
                // removes where peice was
                game.updatePriorPosition(piece);
                passException = true;
            }
            // catches exception if user chooses coordinate where there is no piece
            catch (NullPointerException ex) {
                System.out.println("There is no piece at the coordinates entered\n" +
                "Please enter x and then y coordinate of piece again");
                passException = false;
            }
            // catches exception if user chooses coordiante that is outside of array
            catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("You chose a piece that is outside of the board's coordinate system\n" +
                "Please enter x and then y coordinate of piece again");
                passException = false;
            }
        } while (passException == false);
    }

    public static void userEnterNewCoordinates() {
    // user enters where piece goes
        boolean passException = true;
        System.out.println("Enter x an y coordinate of where you want to move the piece");
        // continuously loops if user chooses invalid coordinate
        do {
            try {
                yMove = input.nextInt();
                xMove = input.nextInt();
                // moves peice to new location
                piece.setXY(xMove, yMove);
                // updates peice position in game
                game.updatePiecePosition(piece);
                passException = true;
            }
            // catches if user tries to move piece outside of board array
            catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("You chose a location that is outside of the board's coordinate system\n" +
                "Please enter x and y coordinate of where you want to the piece");
                passException = false;
            }
        } while (passException == false);
       
    }
}