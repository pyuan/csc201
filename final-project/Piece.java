public abstract class Piece {
    private int x;    
    private int y;
    private String picture;
    private boolean isWhite;

    // set x
    public void setX(int x) {
        this.x = x;
    }

    // set y
    public void setY(int y) {
        this.y = y;
    }

    // set x and y
    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // get x
    public int getX() {
        return x;
    }

    // get y
    public int getY() {
        return y;
    }

    // set UTF character for piece
    public void setPicture(String picture) {
        this.picture = picture;
    }

    // get UTF character for piece
    public String getPicture() {
        return picture;
    }
    
    // set color
    public void setIsWhite(boolean isWhite) {
        this.isWhite = isWhite;
    }

    // get color
    public boolean getIsWhite() {
        return isWhite;
    }
}