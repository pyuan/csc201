public class InvalidTeamException extends Exception {
    
    public InvalidTeamException(Boolean isWhite) {
        String color;
        // checks for color based on parameter
        if (isWhite == true) {
            color = "White";
        }
        else {
            color = "Black";
        }
        // prints out why user can't use that piece
        System.out.println("It is not your turn to move " + color + "'s peice");
    }
}