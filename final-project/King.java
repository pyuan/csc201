public class King extends Piece {

    public King() {
    }

    // constructor with arguments
    public King(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♔");
        }
        else {
            setPicture("♚");
        }

    }
}