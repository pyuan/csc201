public class Rook extends Piece {

    public Rook() {
    }

    public Rook(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♖");
        }
        else {
            setPicture("♜︎");
        }

    }
}