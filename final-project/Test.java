public class Test {

    public static void main(String[] args) {
        Rectangle one = new Rectangle(10, 10);
        Rectangle two = new Rectangle(10, 10);
        Rectangle three = new Rectangle(20, 20);
        System.out.println(one.compareTo(two));
        System.out.println(one.compareTo(three));
        System.out.println(three.compareTo(one));
        System.out.println(one.equals(two));
    }
    
}