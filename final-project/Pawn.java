public class Pawn extends Piece {

    public Pawn() {
    }

    // constructor with arguments
    public Pawn(int x, int y, boolean isWhite) {
        setX(x);
        setY(y);
        setIsWhite(isWhite);
        if (getIsWhite() == true) {
            setPicture("♙");
        }
        else {
            setPicture("♟︎");
        }
    }

}