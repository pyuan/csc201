import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class Test2 {
    public static void main(String[] args) throws IOException {
        new Person().printPerson();
        new Student().printPerson();

    }
}

class Student extends Person {
    private String getInfo() {
        return "student";
    }
}

class Person {
    private String getInfo() {
        return "Person";
    }

    public void printPerson() {
        System.out.println(getInfo());
    }
}