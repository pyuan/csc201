import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class FlowPaneDemo extends Application {

    // override start method in application
    @Override
    public void start(Stage primaryStage) {
        // create FlowPane object
        FlowPane pane1 = new FlowPane();

        // add buttons
        pane1.getChildren().add(new Button("button 1"));
        pane1.getChildren().add(new Button("button 2"));
        pane1.getChildren().add(new Button("button 3"));
        
        // create scene based on FlowPane object pane1 and set to 800x750
        Scene scene1 = new Scene(pane1, 800, 750);

        // set stage window title
        primaryStage.setTitle("ShowFlowPane");

        // set scene1 for stage
        primaryStage.setScene(scene1);

        // shows stage
        primaryStage.show();

        // create second stage
        Stage stage = new Stage();

        // sets second stage window title
        stage.setTitle("Different ShowFlowPane");

        // creates new FlowPane object pane2 with 3 more buttons (shorthand)
        FlowPane pane2 = new FlowPane(new Button("button 4"), new Button("button 5"), new Button("button 6"));

        // creates new scene bassed on FlowPane object pane2 and set to 100x250
        Scene scene2 = new Scene(pane2, 100, 250);

        // set scene2 on stage object
        stage.setScene(scene2);

        // shows stage object or second stage
        stage.show();
    }
    
    public static void main(String[] args) {
        Application.launch(args);
    }
}