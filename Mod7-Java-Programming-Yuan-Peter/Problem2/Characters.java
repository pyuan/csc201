import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class Characters extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // create pane
        Pane pane = new Pane();

        // create font
        Font font = Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35);

        // create string
        String string = "Welcome to Java";

        // create two rotations
        // this rotation controls position
        double rotation = 0;
        // this rotation controlls the character rotation
        double charRotation = 90;

        // x coordinate of center
        double xplus = 135;

        // radius of circle
        double radius = 100;

        // x and y variables used in for loop
        double x, y;

        // for loop that iterates over every character
        for (int i = 0; i < string.length(); i++) {

            // finds coordinate of letter
            x = xplus + Math.cos(Math.toRadians(rotation)) * radius;
            y = xplus + Math.sin(Math.toRadians(rotation)) * radius;

            // find letter based on index
            // also converts char to string
            String s = String.valueOf(string.charAt(i));

            // creates text object based on coordinates and char found
            Text text = new Text(x, y, s);

            // applies font defined earlier to text object
            text.setFont(font);

            // applies char rotation to the text object
            text.setRotate(charRotation);

            // addes text object to pane
            pane.getChildren().add(text);

            // increases "degree of rotation" for both rotation and char rotation
            // 360 degrees divided by length of string + 1 extra whitespace
            rotation += 360 / (string.length() + 1);
            charRotation += 360 / (string.length() + 1);
        }
        // creates scene with pane and size of 300x300
        Scene scene = new Scene(pane, 300, 300);

        // sets scene to primaryStage
        primaryStage.setScene(scene);
        
        // sets window title
        primaryStage.setTitle("Characters go around circle");

        // shows the stage
        primaryStage.show();

    }
}