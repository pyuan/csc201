import java.util.Scanner;

public class CycleThrowTryCatch {
    public static void main(String[] args) {
        //create scanner
        Scanner input = new Scanner(System.in);
            
        try {
            // takes user input
            System.out.println("Enter number of wheels");
            double input1 = input.nextDouble();
            System.out.println("Enter weight");
            double input2 = input.nextDouble();

            // creates Cycle instance
            Cycle driver1 = new Cycle(input1, input2);

            // print it accessing toString() method
            System.out.println("Success!\n" + driver1.toString());
        }
        // catches exception and prints erro
        catch (IllegalArgumentException ex){
            //prints error
            System.out.println("Something went wrong!\n" + ex.toString());
        }
        input.close();

    }
}

class Cycle {
     //declare variables
    private double numberOfWheels, weight;
    
    Cycle(double numberOfWheels, double weight) {
        if (numberOfWheels <= 0 || weight <= 0) {
            throw new IllegalArgumentException("Values cannot be less than or equal to zero");
        }
        else {
            // construcuts cycle object with its attributes = arguments
            this.numberOfWheels = numberOfWheels;
            this.weight = weight;
        }
    }

    public String toString() {
        // toStirng() that returns class objects objects in string form
        // number of wheels is casted to an int because you cant have a decimal wheel
        return "There are " + (int)numberOfWheels + " wheels and they weigh " + weight + " units"; 
    }

}
