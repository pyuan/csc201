import java.io.IOException;
import java.io.File;
import java.util.Scanner;

public class CycleFileInput {
    public static void main(String[] args) throws IOException {
            
        try {
            // create file object with Cycle.txt
            File file = new File("Cycle.txt");

            // checks if the file exists or not
            System.out.println("Does Cycle.txt exist? " + file.exists());

            // create scanner that gets input from file(Cycle.txt)
            Scanner input = new Scanner(file);

            // create new Cycle object based on Cycle.txt's contents
            Cycle driver1 = new Cycle(input.nextInt(), input.nextInt());

            // prints the toString() method of Cycle to display driver1's contents
            System.out.println(driver1.toString());

            // close the file
            input.close();
        }
        // catches exception and prints erro
        catch (IOException ex) {
            //prints error
            System.out.println("You have an IOException. Something went wrong with input and output");
        }

    }
}

class Cycle {
     //declare variables
    private int numberOfWheels, weight;
    
    Cycle(int numberOfWheels, int weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() {
        // toStirng() that returns class objects objects in string form
        return "Number of wheels: " + numberOfWheels + "\nWeight: " + weight + " units"; 
    }

}