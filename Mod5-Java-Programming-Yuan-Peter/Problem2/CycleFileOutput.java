import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;

public class CycleFileOutput {
    public static void main(String[] args) throws IOException {
        try {
            // create Cycle instance
            Cycle driver1 = new Cycle(4, 100);

            // create file that will use Cycle.txt
            File file = new File("Cycle.txt");

            // preapres printwriter to write to file
            PrintWriter output = new PrintWriter(file);

            // prints out if Cycle.txt exists or not
            System.out.println("Does Cycle.txt exist? " + file.exists());

            // writes the toString of Cycle to Cycle.txt
            output.print(driver1.toString());

            // closes output object
            output.close();
        }
        catch (IOException ex) {
            System.out.println("You have an IOException. Something went wrong with input and output");
        }
    }
}

class Cycle {
     //declare variables
    private int numberOfWheels, weight;
    
    Cycle(int numberOfWheels, int weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() {
        // toStirng() that returns class objects objects in string form
        return "Number of wheels: " + numberOfWheels + "\nWeight: " + weight + " units"; 
    }

}