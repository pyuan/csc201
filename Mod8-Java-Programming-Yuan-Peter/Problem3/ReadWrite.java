import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ReadWrite {
    public static void main(String[] args) throws IOException {
        // create scanner input object
        Scanner input = new Scanner(System.in);

        try {
            try (
                // create random access file with file Stu.dat as read and write
                RandomAccessFile raf = new RandomAccessFile("Stu.dat", "rw");
            ) {
                // clear Stu.dat of any previous data
                raf.setLength(0);

                // prompt user for how many students there are
                System.out.println("How many students are there? ");
                int studentCount = input.nextInt();

                // creates array based on user input
                int[] studentIDList = new int[studentCount];
                double[] gpaList = new double[studentCount];

                // interactively assign student ids and gpas to array and write to Stu.dat
                for (int i = 0; i < studentCount; i++) {
                    // prompt user for student id and gpa
                    System.out.println("Enter the student ID and GPA for student " + (i + 1) + 
                    "\nStudent IDs must be unique");
                    studentIDList[i] = input.nextInt();
                    gpaList[i] = input.nextDouble();

                    // write student id and gpa to Stu.dat
                    raf.writeInt(studentIDList[i]);
                    raf.writeDouble(gpaList[i]);
                }

                int sentinel = 1;
                // loop controlled by sentinel
                while (sentinel == 1) {
                    // list student ids
                    System.out.println("These are the student ids");
                    for (int i = 0; i < studentCount; i++) {
                        System.out.println(studentIDList[i]);
                    }

                    // prompt user for student id
                    System.out.println("Type out the studentID of the student whose GPA you want to see");
                    int tempID = input.nextInt();

                    // create sentinel for second while loop
                    String continue_yn = "y";

                    // loop over Stu.dat
                    while (continue_yn == "y") {
                        // looks for id entered by user
                        for (int i = 0; i < studentCount; i++) {
                            // moves file pointer to int student id
                            raf.seek(i * 12);

                            // if user entered id found
                            if (tempID == raf.readInt()) {
                                // print out student id and gpa
                                System.out.println(tempID + "'s GPA is " + raf.readDouble());
                                // moves file pointer to spot so appropriate gpa is printed
                                raf.seek(i * 12 + 12);
                                // loop is terminated
                                continue_yn = "n";
                            }
                        }
                    }
                    // prompt user if they want to continue or not
                    System.out.println("Type 1 to continue or 0 to exit");
                    sentinel = input.nextInt();
                }
                // close random access file object
                raf.close();
            }
        }
        // catch exceptions
        catch (IOException ex) {
            System.out.println("There was an IOException. Check your input and output streams\nThis is the stack trace\n" +
            // print stack trace
            ex.getStackTrace());
        }
        finally {
            // close Scanner object
            input.close();
            System.out.println("Thank you for using this software!");
        }
    }
}