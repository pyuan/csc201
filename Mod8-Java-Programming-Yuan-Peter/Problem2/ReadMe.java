import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.File;

public class ReadMe {
    public static void main(String[] args) throws IOException {
        System.out.println("This prints the characters at position 125 and 134");
        try {
            // open file Pledge.txt
            File file = new File("Pledge.txt");
            try (
                // create random access file object
                RandomAccessFile inout = new RandomAccessFile(file, "rw");
            ) {
                System.out.println("Position 125:");
                // seek to position 125
                inout.seek(125);
                // print
                System.out.println(inout.readLine());
                System.out.println("Position 134:");
                // seek to position 125
                inout.seek(134);
                // print
                System.out.println(inout.readLine());
                // close random access object
                inout.close();
            }
        }
        // catch exceptions
        catch (IOException ex) {
            System.out.println("There was an IOException. Check your input and output streams. Here is the stacktrace\n" + ex.getStackTrace());
        }
        // finally block to thank the user
        finally {
            System.out.println("Thank you for using this software");
        }
    }
}