import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.io.File;

public class Fraction implements Serializable {
    private int numerator, denominator;
    
    // no arg constructor
    public Fraction() {
    }

    // constructor that takes two integers as input
    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        // conditional to test if denominator is 0
        if (denominator == 0) {
            System.out.println("Denominator cannot be 0");
        }
        else {
            this.denominator = denominator;
        }
    }

    // set method for numerator
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    // set method for denominator
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    // get method for numerator 
    public int getNumerator() {
        return numerator;
    }

    // get method for denominator
    public int getDenominator() {
        return denominator;
    }

    public static void main(String[] args) throws IOException {
        // create array that hold refrences to Fraction objects
        Fraction[] fractionList = new Fraction[3];

        try {
            // create file
            File file = new File("SerialF.dat");
            try (
                // create ObjectOutputStream
                ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file));
            ) {
                // Initializes 3 fraction objects with random numberator and denominator integers
                for (int i = 0; i < 3; i++) {
                    // initialization
                    fractionList[i] = new Fraction((int)(Math.random()*10 + 1), (int)(Math.random()*10 + 1));
                    // writes object to SerialF.dat
                    output.writeObject(fractionList[i]);
                }
                // closes output file
                output.close();
            }
        }
        // catches IOException
        catch (IOException ex) {
            // prints message and stack trace
            System.out.println("There was an IOException. Check to make sure your input and output streams are correct" +
            "\nHere is the stack trace\n" + ex.getStackTrace());
        }
    }

}