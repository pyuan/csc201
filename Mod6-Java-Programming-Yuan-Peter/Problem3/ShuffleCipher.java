public class ShuffleCipher implements MessageEncoder, MessageDecoder {
    private int shuffleNumber;

    public ShuffleCipher() {
    }

    public ShuffleCipher(int n) {
        shuffleNumber = n;
    }

    public String encode(String plainText) {
        String toEncode = plainText;
        for (int i = 0; i < shuffleNumber; i++) {
            toEncode = shuffle(toEncode);
        }
        return toEncode;
    }
    public String decode(String cipherText) {
        String toDecode = cipherText; 
        for (int i = 0; i < shuffleNumber; i++) {
            toDecode = unShuffle(toDecode);
        }
        return toDecode;
    }
    private static String shuffle(String plainText) {
        int stringLength = plainText.length();
        String half1, half2;
        if (stringLength % 2 == 0) {
            half1 = plainText.substring(0, stringLength/2);
            half2 = plainText.substring(stringLength/2, stringLength);
        }
        else {
            half1 = plainText.substring(0, stringLength/2 + 1);
            half2 = plainText.substring(stringLength/2 + 1, stringLength);
        }

        char[] newString = new char[stringLength];

        int count1 = 0;
        int count2 = 0;

        for (int i = 0; i < newString.length; i++) {
            if (i % 2 == 0) {
                newString[i] = half1.charAt(count1);
                count1++;
            }
            else {
                newString[i] = half2.charAt(count2);
                count2++;
            }
        }
        
        String encoded = new String(newString);
        return encoded;
    }

    private static String unShuffle(String cipherText) {

        int stringLength = cipherText.length();
        char[] half1;
        char[] half2 = new char[stringLength / 2];
        int count1 = 0;
        int count2 = 0;

        if (stringLength % 2 == 0) {
            half1 = new char[stringLength / 2];
        }
        else {
            half1 = new char[stringLength / 2 + 1];
        }

        for (int i = 0; i < stringLength; i++) {
            if (i % 2 == 0) {
                half1[count1] = cipherText.charAt(i);
                count1++;
            }
            else {
                half2[count2] = cipherText.charAt(i);
                count2++;
            }
        }
        String part1 = new String(half1);
        String part2 = new String(half2);
        return part1 + part2;
    }
}