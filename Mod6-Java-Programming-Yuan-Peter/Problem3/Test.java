public class Test {
    public static void main(String[] args) {
        SubstitutionCipher cipher1 = new SubstitutionCipher(1);
        String text = cipher1.encode("abc");
        System.out.println(text);
        String afterDecode = cipher1.decode(text);
        System.out.println(afterDecode);

        ShuffleCipher cipher2 = new ShuffleCipher(3);
        String toEncode = cipher2.encode("abcdefghijklmnop");
        System.out.println("\n" + toEncode);
        String afterDecode2 = cipher2.decode(toEncode);
        System.out.println(afterDecode2);

    }

}