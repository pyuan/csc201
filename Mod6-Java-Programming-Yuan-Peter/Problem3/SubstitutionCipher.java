public class SubstitutionCipher implements MessageEncoder, MessageDecoder {
    private int shift;

    SubstitutionCipher() {
    }

    SubstitutionCipher(int shift) {
        this.shift = shift;
    }

    public String encode(String plainText) {
        return shift(shift, plainText);
    }

    public String decode(String cipherText) {
        return unShift(shift, cipherText);
    }

    private static String shift(int shift, String plainText) {
        char[] toEncode = plainText.toCharArray();
        for (int i = 0; i < toEncode.length; i++) {
            int temp = (toEncode[i] + shift - (int)'a') % 26 + (int)'a';
            toEncode[i] = (char)temp;
        }
        String encoded = new String(toEncode);
        return encoded;
    }

    private static String unShift(int shift1, String cipherText) {
        return shift(-1 * shift1, cipherText);
    }
}