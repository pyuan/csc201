import java.util.Scanner;

public class Problem3Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String encodedMessage = "";
        String decodedMessage = "";
        SubstitutionCipher cipher1 = new SubstitutionCipher();
        ShuffleCipher cipher2 = new ShuffleCipher();

        // ask user about which cipher to make
        System.out.println("This application allows you to encode and decode messages." + 
        "\nPress 1 to make a substitution cipher or 2 to make a shuffle cipher. Then enter the number of shifts or shuffles");
        int cipherType = input.nextInt();
        int numberOfChanges = input.nextInt();
        input.nextLine();

        // create cipher
        if (cipherType == 1) {
            cipher1 = new SubstitutionCipher(numberOfChanges);
        }
        else if (cipherType == 2) {
            cipher2 = new ShuffleCipher(numberOfChanges);
        }
        else {
            System.out.println("Not a valid response. Goodbye!");
            System.exit(1);
        }

        // enter message
        System.out.println("Enter your message");
        String plainTextMessage; 
        plainTextMessage = input.nextLine();
        
        // encode message
        if (cipherType == 1) {
            encodedMessage = cipher1.encode(plainTextMessage);
        }
        else if (cipherType == 2) {
            encodedMessage = cipher2.encode(plainTextMessage);
        }

        // print encoded message
        System.out.println("Your encoded message is: " + encodedMessage);


        // choose to print decoded message or not
        System.out.println("If you would like to decode it hit 1, if you would like to exit hit 0");
        int decodeChoice = input.nextInt();

        // decodes
        if (decodeChoice == 1 && cipherType == 1) {
            decodedMessage = cipher1.decode(encodedMessage);
        }
        else if (decodeChoice == 1 && cipherType == 2) {
            decodedMessage = cipher2.decode(encodedMessage);
        }
        else {
            System.out.println("Goodbye!");
            System.exit(1);
        }
        
        // prints out decoded message
        System.out.println("Your decoded message is: " + decodedMessage);
        System.out.println("Thank you for using this program");
        input.close();
    }
    
}