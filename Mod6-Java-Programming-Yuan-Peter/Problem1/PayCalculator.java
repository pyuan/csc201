public abstract class PayCalculator {
    private double payRate;

    public double computePay(double hours) {
        return payRate * hours;
    }

    public void setPayRate(double payRate) {
        this.payRate = payRate;
    }

    public double getPayRate() {
        return payRate;
    }
}