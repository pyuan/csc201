public class HazardPay extends PayCalculator {

    public HazardPay() {
    }

    public HazardPay(double payRate) {
        setPayRate(payRate);
    }

    @Override
    public double computePay(double hours) {
        return super.computePay(hours) * 1.5;
    }
}