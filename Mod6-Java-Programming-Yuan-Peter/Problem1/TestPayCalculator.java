public class TestPayCalculator {
    public static void main(String[] args) {
        RegularPay minimumWage = new RegularPay(8);
        HazardPay waiters = new HazardPay(8);

        System.out.println("People who work minimum wage are paid " + 
        minimumWage.computePay(8) + 
        " dollars after working 8 hours\n" +
        "People who work as waiter on a good day earn " + 
        waiters.computePay(8) + 
        " dollars because they get tips");
    }
}