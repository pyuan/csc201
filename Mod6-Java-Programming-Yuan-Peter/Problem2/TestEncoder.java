public class TestEncoder {
    public static void main(String[] args) {
        SubstitutionCipher cipher1 = new SubstitutionCipher(3);
        String encodedMessage1 = cipher1.encode("abcdef");
        System.out.println("Substituion Cipher with a shift of 3:\n'abcdef' is " + encodedMessage1);


        ShuffleCipher cipher2 = new ShuffleCipher(2);
        String encodedMessage2 = cipher2.encode("racecar");
        System.out.println("Shuffle Cipher shuffled 2 times:\n'racecar' is " + encodedMessage2);

        ShuffleCipher cipher3 = new ShuffleCipher(3);
        String encodedMessage3 = cipher3.encode("racecar");
        System.out.println("Shuffle Cipher shuffled 3 times:\n'racecar' is " + encodedMessage3);
    }
}