public class SubstitutionCipher implements MessageEncoder {
    private int shift;

    SubstitutionCipher() {
    }

    SubstitutionCipher(int shift) {
        this.shift = shift;
    }

    public String encode(String plainText) {
        return shift(shift, plainText);
    }

    private static String shift(int shift, String plainText) {
        char[] toEncode = plainText.toCharArray();
        for (int i = 0; i < toEncode.length; i++) {
            int temp = (toEncode[i] + shift - (int)'a') % 26 + (int)'a';
            toEncode[i] = (char)temp;
        }
        String encoded = new String(toEncode);
        return encoded;
    }
}