public class ShuffleCipher implements MessageEncoder {
    private int shuffleNumber;

    public ShuffleCipher() {
    }

    public ShuffleCipher(int n) {
        shuffleNumber = n;
    }

    public String encode(String plainText) {
        String encoding = plainText;
        for (int i = 0; i < shuffleNumber; i++) {
            encoding = shuffle(encoding);
        }
        return encoding;
    }

    private static String shuffle(String plainText) {
        int stringLength = plainText.length();
        String half1, half2;
        if (stringLength % 2 == 0) {
            half1 = plainText.substring(0, stringLength/2);
            half2 = plainText.substring(stringLength/2, stringLength);
        }
        else {
            half1 = plainText.substring(0, stringLength/2 + 1);
            half2 = plainText.substring(stringLength/2 + 1, stringLength);
        }

        char[] newString = new char[stringLength];

        int count1 = 0;
        int count2 = 0;

        for (int i = 0; i < newString.length; i++) {
            if (i % 2 == 0) {
                newString[i] = half1.charAt(count1);
                count1++;
            }
            else {
                newString[i] = half2.charAt(count2);
                count2++;
            }
        }
        
        String encoded = new String(newString);
        return encoded;

    }
}