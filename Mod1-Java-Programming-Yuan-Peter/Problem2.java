import java.util.Scanner;

public class Problem2 {
	/*
	pseudocode
	for loop over what items they want
		loop continues if user wants to
		maximum items is 5
	while loop over if price has been paid or not
		user chooses what types of money to use
		amount is subtracted from price and added to refund
	*/
    public static void main(String[] args) { 

		// initialize variables and array
		double soda = 2.25, chips = 5.80, water = 0.99, bar = 1.83, popcorn = 4.36, price = 0.00;
        System.out.print
            ("Welcome to the vending machine\n");
		Scanner input = new Scanner(System.in);
		int chooseAgain = 1;
		String[] items_bought = {" ", " ", " ", " ", " "};

		// buying items
		for (int items = 0; items < 5 && chooseAgain == 1; ++items) {
			System.out.print("item 1 is soda, item 2 is chips, item 3 is water, item 4 is bar, item 5 is popcorn, hit 0 to cancel\n");
        	int want = input.nextInt();
			if (want == 1) {
				price += soda;
				items_bought[items] = "soda";
			}
			else if (want == 2) {
				price += chips;
				items_bought[items] = "chips";
			}
			else if (want == 3) {
				price += water;
				items_bought[items] = "water";
			}
			else if (want == 4) {
				price += bar;
				items_bought[items] = "bar";
			}
			else if (want == 5) {
				price += popcorn;
				items_bought[items] = "popcorn";
			}
			else if (want == 0) {
				System.out.println("You have chosen not to buy anything\nGoodbye!");
				chooseAgain = 0;
				System.exit(0);
			}
			else {
				System.out.println("That is not an item");
			}

			System.out.println("Enter 1 to buy another item or another integer to continue to payment");
			chooseAgain= input.nextInt();
		}
		System.out.println("The price is $" + price);

		// paying for items
		double refund = 0;
		int refund_yn = 0;
		while (price > 0) {
			System.out.printf("You still have to pay %.2f", price);
			System.out.println("\nEnter 1 to pay $5, 2 to pay $1, 3 to pay a quarter, 4 to pay a dime, 5 to pay a nickel, 6 to pay a penny, hit 0 to cancel and get refund");
			int payment = input.nextInt();
			if (payment == 1) {
				price -= 5.00;
				refund += 5.00;
			}
			else if (payment == 2) {
				price -= 1.00;
				refund += 1.00;
			}
			else if (payment == 3) {
				price -= 0.25;
				refund += 0.25;
			}
			else if (payment == 4) {
				price -= 0.10;
				refund += 0.10;
			}
			else if (payment == 5) {
				price -= 0.05;
				refund += 0.05;
			}
			else if (payment == 6) {
				price -= 0.01;
				refund += 0.01;
			}
			else if (payment == 0) {
				refund_yn = 0;
				price = 0;
			}
			else {
				System.out.println("Invalid payment option");
			}
		}

		System.out.println("if you would like to cancel the transaction, hit 0");
		refund_yn = input.nextInt();

		// giving change
		if (refund_yn == 0) {
			double converted = refund * 100;
			int convert = Math.abs((int)converted);
			int quarters = convert / 25;
			int dimes = (convert % 25) / 10;
			int nickels = ((convert % 25) % 10) / 5;
			int pennies = ((convert % 25) % 10) % 5;
			System.out.println("\nYour refund in coins is " + quarters + " quarters " + dimes + " dimes " + nickels + " nickels " + pennies + " pennies");

			System.out.println("Your returned total is $" + refund);
		}
		else if (price < 0.00) {
			System.out.println("Your items are");
			for (int i = 0; i < items_bought.length; ++i) {
				System.out.print(items_bought[i] + " ");
			}
			double converted = price * 100;
			int convert = Math.abs((int)converted);
			int quarters = convert / 25;
			int dimes = (convert % 25) / 10;
			int nickels = ((convert % 25) % 10) / 5;
			int pennies = ((convert % 25) % 10) % 5;
			System.out.println("\nYour change is " + quarters + " quarters " + dimes + " dimes " + nickels + " nickels " + pennies + " pennies");
		}
		else {
			System.out.println("You have paid. Your items are");
			for (int i = 0; i < items_bought.length; ++i) {
				System.out.print(items_bought[i] + " ");
			}
		}
		input.close();
	}
}
