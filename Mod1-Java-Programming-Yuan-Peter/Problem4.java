import java.util.Scanner;

public class Problem4 {
    /*
    Ask user for input of which operation
    Ask user for real and imaginary components
    Conditional based on which operation
    Conditional calls method based on operation
    real and imaginary components calculated
    method prints answer and formula used to solve
    */
   public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 0 to add, 1 to subtract, 2 to multiply, 3 to divide");
        int choice = input.nextInt();
        String sign = "";
        switch (choice) {
            case 0: sign = "+"; break;
            case 1: sign = "-"; break;
            case 2: sign = "*"; break;
            case 3: sign = "/"; break;
        }
        System.out.println("What is your first expression's real number?");
        double a1 = input.nextDouble();
        System.out.println("What is your first expression's imaginary number?");
        double b1 = input.nextDouble();
        System.out.println("What is your second expression's real number?");
        double a2 = input.nextDouble();
        System.out.println("What is your second expression's imaginary number?");
        double b2 = input.nextDouble();
        System.out.println("(" + a1 + " + (" + b1 + "i)) " + sign + " (" + a2 + " + (" + b2 + "i)) =");
        input.close(); 
        if (choice == 0) {
            addition(a1, b1, a2, b2);
        }
        else if (choice == 1) {
            subtraction(a1, b1, a2, b2);
        }
        else if (choice == 2) {
            multiply(a1, b1, a2, b2);
        }
        else if (choice == 3) {
            divide(a1, b1, a2, b2);
        }
        else {
            System.out.println("The number for choice was not valid");
        }
   }

   public static void addition(double a1, double b1, double a2, double b2) {
        double zreal = a1 + a2;
        double zimag = b1 + b2;
        System.out.println(zreal + " + " + zimag + "i");
        System.out.println("Solved with z1 + z2 = (a1 + a2) + i * (b1 + b2)");
   }

   public static void subtraction(double a1, double b1, double a2, double b2) {
       double zreal = a1 - a2;
       double zimag = b1 - b2;
       System.out.println(zreal + " + " + zimag + "i");
       System.out.println("Solved with z1 - z2 = (a1 - a2) + i * (b1 - b2)");
   }

   public static void multiply(double a1, double b1, double a2, double b2) {
       double zreal = ((a1 * a2) - (b1 * b2));
       double zimag = ((a1 * b2) - (b1 * a2));
       System.out.println(zreal + " + " + zimag + "i");
       System.out.println("Solved with z1 * z2 = (a1 * a2 - b1 * b2) + i * (a1 * b2 + b1 * a2)");
   }

   public static void divide(double a1, double b1, double a2, double b2) {
       double zreal = ( ((a1 * a2) + (b1 * b2)) / (Math.pow(a2, 2) + Math.pow(b2, 2)) );
       double zimag = ( ((b1 * a2) - (a1 * b2)) / (Math.pow(a2, 2) + Math.pow(b2, 2)) );
       System.out.println(zreal + " + " + zimag + "i");
       System.out.println("Solved with z1 / z2 = (a1 * a2 + b1 * b2) / (a2^2 + b2^2) + i * (b1 * a2 - a1 * b2) / (a2^2 + b2^2)");
   }
}