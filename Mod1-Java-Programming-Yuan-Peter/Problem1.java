import java.util.Scanner;

public class Problem1 {
    /*
    pseudocode
    ask for studentid
    concatenate string with returned values from methods
    method findChapter 
        studentid divided by 3, remainder incremented by 3
        if == 4 then go to chapter 6
    method findProblem
        conditional based on chapter number
        find remainder and the increment
    */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("What is your student ID?");
        int studentID = input.nextInt();
        System.out.println("Your chaper is " + findChapter(studentID) + " and your problem is " + findProblem(findChapter(studentID), studentID));
        input.close();
    }

    public static int findChapter(int n) {
        int afterMath = (n % 3) + 3;
        if (afterMath == 4) {
            return 6;
        }
        else {
            return afterMath;
        }
    }

    public static int findProblem(int chapter, int ID) {
        if (chapter == 3) {
            return (ID % 34) + 1;
        }
        else if (chapter == 6){
            return (ID % 38) + 1;
        }
        else {
            return (ID % 46) + 1;
        }
    }
}