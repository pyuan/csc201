public class Problem3 {
   /*
   pseudocode
   Use Math.random to generate random numbers
   Modulus 4 to get 4 possibilities or directions
   Switch statement to choose direction
   60 / 5 = 12 so change happens 12 times in an hour
   Increment by 1
   Multiple by 5 to get real distance
   Use distance formula to calculate distance from origin
   */
    public static void main(String[] args) {
       System.out.println("The car started at (0,0)");
       int x = 0, y = 0;
       for (int i = 0; i < 12; ++i) {
            switch ((int)(Math.random() * 1000) % 4) {
                // North
                case 0: ++y; break;
                // East
                case 1: ++x; break;
                // South
                case 2: --y; break;
                // West
                case 3: --x; break;
            }
       }
       x = x * 5;
       y = y * 5;
       System.out.println("After 1 hour, the car is at (" + x + "," + y + ")");
       double distance = Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2)));
       System.out.printf("The car traveled %.2f miles\n", distance);
   }
}