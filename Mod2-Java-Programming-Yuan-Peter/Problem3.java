import java.util.Scanner;

public class Problem3 {
    /*
    pseudocode
    inputTempForMonth
    create scanner object
    array[month or i in inputTempForYear][0 or 1 depending on high or low]
    user inputs doubles

    inputTempForYear
    create array that has 12 rows and 2 columns
    loop over each row or month calling inputTempForMonth

    calculateAverageHigh
    create total variable
    loop over array adding all values of column 0
    return total divided by 12

    calculateAverageLow 
    same as calculateAverageHigh except for column 1

    findHigestTemp
    create max and index variable
    loop through column 0
    if a larger number is found than index 0, max = new highest temp and index variable becomes current index
    return index

    findLowestTemp
    same as findHighestTemp except looks for in column 1 and checks if temperture is lower

    main
    call each method when needd
    have array of string months
    properly concatenate method results

    */
    public static void main(String[] args) {
        double[][] yearHighAndLow = inputTempForYear();
        int highestTempIndex = findHighestTemp(yearHighAndLow);
        int lowestTempIndex = findLowestTemp(yearHighAndLow);
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        System.out.println("The monthly high average is " + calculateAverageHigh(yearHighAndLow) + " degrees\n" 
        + "The monthly low average is " + calculateAverageLow(yearHighAndLow) + " degrees\n"
        + "The month with the highest temperature is " + months[highestTempIndex] + " at " + yearHighAndLow[highestTempIndex][0] + " degrees\n"
        + "The month with the lowest temperature is " + months[lowestTempIndex] + " at " + yearHighAndLow[lowestTempIndex][1] + " degrees");

    }
    public static void inputTempForMonth(int month, double[][] yearHighAndLow) {
        Scanner input = new Scanner(System.in);
        yearHighAndLow[month][0] = input.nextDouble();
        yearHighAndLow[month][1] = input.nextDouble();
        input.close();

    }
    public static double[][] inputTempForYear() {
        double[][] yearHighAndLow = new double[12][2];
        System.out.println("Input the monthly high and then the monthly low for each month starting at january");
        for (int row = 0; row < yearHighAndLow.length; ++row) {
            inputTempForMonth(row, yearHighAndLow);
        }
        return yearHighAndLow;
    }
    public static double calculateAverageHigh(double[][] yearHighAndLow) {
        double highTotal = 0;
        for (int i = 0; i < yearHighAndLow.length; ++i) {
            highTotal += yearHighAndLow[i][0];
        }
        return highTotal / 12.0;
    }
    public static double calculateAverageLow(double[][] yearHighAndLow) {
        double lowTotal = 0;
        for (int i = 0; i < yearHighAndLow.length; ++i) {
            lowTotal += yearHighAndLow[i][1];
        }
        return lowTotal / 12.0;
    }
    public static int findHighestTemp(double[][] yearHighAndLow) {
        double max = yearHighAndLow[0][0];
        int indexOfMax = 0;
        for (int i = 0; i < yearHighAndLow.length; ++i) {
            if (yearHighAndLow[i][0] > max) {
                max = yearHighAndLow[i][0];
                indexOfMax = i;
            }
        }
        return indexOfMax;
    }
    public static int findLowestTemp(double[][] yearHighAndLow) {
        double min = yearHighAndLow[0][1];
        int indexOfMin = 0;
        for (int i = 0; i < yearHighAndLow.length; ++i) {
            if (yearHighAndLow[i][1] < min) {
                min = yearHighAndLow[i][1];
                indexOfMin = i;
            }
        }
        return indexOfMin;
    }

}