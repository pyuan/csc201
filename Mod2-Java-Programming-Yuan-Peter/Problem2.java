import java.util.Scanner;

public class Problem2 {
    /*
    psuedocode
    inputAllScore 
    creates array that holds judges scores
    changes values according to user by calling inputValidScore

    calculateScore
    find lowest and highest score from judge scores array
    loops to find sum of all scores
    subtract lowest and highest from the sum of all scores
    multiply by degree of difficulty found in inputValidDegreeDifficulty and 0.6
    return score

    main 
    calls methods that satisfy method calculateScore
    */
    public static void main(String[] args) {
        System.out.println(calculateScore(inputAllScore(), inputValidDegreeOfDifficulty()));
    }
    public static double inputValidScore() {
        Scanner input = new Scanner(System.in);
        double validScore = input.nextDouble(); 
        return validScore;
    }
    public static double[] inputAllScore() {
        Scanner input = new Scanner(System.in);
        double[] judgeScores = {0,0,0,0,0,0,0};
        for (int i = 0; i < judgeScores.length; ++i) {
            System.out.print("Judge #" + (i + 1) + " score\t");
            judgeScores[i] = inputValidScore();
            if (judgeScores[i] < 0 || judgeScores[i] > 10) {
                System.out.println("You input an invalid score. Please try again");
                System.exit(0);
            }
        }
        return judgeScores;
    }
    public static double inputValidDegreeOfDifficulty() {
        Scanner input = new Scanner(System.in);
        System.out.print("What is the degree of difficulty (between 1.2 and 3.8)? ");
        double degreeDifficulty = input.nextDouble();
        if (degreeDifficulty < 1.2 || degreeDifficulty > 3.8) {
            System.out.println("You input an invalid difficulty");
            System.exit(0);
        }
        return degreeDifficulty;
    }
    public static double calculateScore(double[] judgeScores, double degreeDifficulty) {
        double max = 0;
        for (int i = 0; i < judgeScores.length; ++i) {
            if (judgeScores[i] > max) max = judgeScores[i];
        }
        double min = 10;
        for (int i = 0; i < judgeScores.length; ++i) {
            if (min > judgeScores[i]) {
                min = judgeScores[i];
            }
        }
        double total = 0;
        for (int i = 0; i < judgeScores.length; ++i) {
            total += judgeScores[i];
        }
        double trueScore = (total - max - min) * degreeDifficulty * 0.6;
        System.out.print("The final score is ");
        return trueScore;
    }
};