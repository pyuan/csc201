import java.util.Scanner;

public class Problem1 {
    /*
    pseudocode
    ask for how many girl scouts
    then create array based on answer
    ask user to input boxes sold for each girl scout
    for each value in array check between box of cookies range
    if in range then increment corresponding range counter
    print results
    */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("How many girls are selling girl scout cookies?");
        int numberOfScouts = input.nextInt();
        String[] rangeList = {" 0 to 10", "11 to 20", "21 to 30", "31 to 30", "41 or more"};

        int[] cookieList = new int[numberOfScouts]; 
        System.out.println("Enter how many boxes of cookies each scout sold?");
        for (int i = 0; i < numberOfScouts; ++i) {
            System.out.print("Boxes of cookies for girl #" + (i + 1) + "\t");
            cookieList[i] = input.nextInt();
        }
        input.close();

        int[] countList = {0, 0, 0, 0, 0};
        for (int i: cookieList) {
            if (i >= 0 && i <= 10)
                countList[0]++;
            else if (i >= 11 && i <= 20)
                countList[1]++;
            else if (i >= 21 && i <= 30)
                countList[2]++;
            else if (i >= 31 && i <= 40)
                countList[3]++;
            else if (i >= 41)
                countList[4]++;
            else {
                System.out.println("Invalid number");
                System.exit(0);
            }
        }
        System.out.println("\nTotal Boxes\tNumber of Girl Scouts");
        for (int i = 0; i < 5; ++i) {
            System.out.println(rangeList[i] + "\t" + countList[i]);
        }


    }
}

