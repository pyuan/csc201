public class Problem2PartB {
    public static void main(String[] args) {
         // create cycle object with no arg constructor
        Cycle driver1 = new Cycle();
        // prints string representation of object
        System.out.println(driver1.toString()); 
    }
}

class Cycle { 
    // ignore warning, compile this without compiling PartB and vice versa
    private int numberOfWheels, weight;

    public Cycle() {
        // defined in problem
        numberOfWheels = 100; 
        weight = 1000;
    }
    
    public Cycle(int numberOfWheels, int weight) {
         // constructor with arguments
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() { 
        // toString method that returns string representaiton of object
        return "There are " + numberOfWheels + " wheels and they weigh " + weight;
    }

}