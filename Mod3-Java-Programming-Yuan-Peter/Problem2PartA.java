public class Problem2PartA {
    public static void main(String[] args) {
        Cycle driver1 = new Cycle(4, 800); // create cycle with 4 wheels that weight 800 units
        System.out.println(driver1.toString()); // print it accessing toString() method
    }
}

class Cycle {
    private int numberOfWheels, weight; //declare variables
    
    Cycle(int numberOfWheels, int weight) {
        this.numberOfWheels = numberOfWheels; // construcuts cycle object with its attributes = arguments
        this.weight = weight;
    }

    public String toString() { // toStirng() that returns class objects objects in string form
        return "There are " + numberOfWheels + " wheels and they weigh " + weight; 
    }

}