public class DemoVolume {
    public static void main(String[] args) {

        // create obeject named volume1 with title volume1 and 3 books
        Volume volume1 = new Volume("Volume 1", 3);         

        // create array of book objects
        Book[] bookList = new Book[3];         

        // print volume object information
        System.out.println(volume1.toString() + "\n"); 

        // define information for each book object
        bookList[0] = new Book("The First Book", "John Smith", 100); 
        bookList[1] = new Book("The Second Book", "John Smith", 250);
        bookList[2] = new Book("The Last Book", "John Smith", 300);

        // print information for every book
        System.out.println(volume1.getBookArray(bookList));
    }
}

class Book {
    private String title, author;
    private int numberOfPages;

    Book() { 
        // no arg constructor
        title = "The First Book";
        author = "John Smith";
        numberOfPages = 10;
    }

    Book(String title, String author, int numberOfPages) { 
        // declares object attribute value = to arguments
        this.title = title;
        this.author = author;
        this.numberOfPages = numberOfPages;
    }

    public String toString() { 
        // string representation of book object
        return "Title: " + title + "\nAuthor: " + author + "\nNumber of pages: " + numberOfPages + "\n";
    }
}
class Volume {
    private String volumeName = "";
    private String bookStuff = "";
    private int numberOfBooks;

    Volume() { 
        // no arg constructor
    }

    Volume(String volumeName, int numberOfBooks) { 
        // declares object attribute value = to arguments
        this.volumeName = volumeName;
        this.numberOfBooks = numberOfBooks;
    }

    public String toString() { // string representation of volume object
        return "Volume Name: " + volumeName + "\nNumber of Books: " + numberOfBooks;
    }

    public String getBookArray(Book[] bookList) {
        // call Book class toString method for every book in bookList
        // returns string representation of every Book object in array
        for (int i = 0; i < bookList.length; i++) { 
            bookStuff += bookList[i].toString();
        }
        return bookStuff;
    }
}