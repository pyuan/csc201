import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {

        // create scanner input
        Scanner input = new Scanner(System.in);

        // create builder object
        Builder string1 = new Builder("Java is fun. ");

        // append string
        string1.doAppend("I love it!");

        // prompt to type
        System.out.println("Type 'Yes, '");

        // takes user input
        String yes = input.nextLine();

        // inserts yes at 13th character
        string1.doInsert(13, yes);
        
        // prints out final string
        System.out.println("The output of the string is:\n" + string1.toString());

        // prints out capapcity
        System.out.println("The capcity of the object is " + string1.getCapacity());

        // close scanner input
        input.close();

    }
} 

class Builder {
    // create StringBuilder object
    StringBuilder event = new StringBuilder();

    public Builder() {
        // no arg constructor
    }

    public Builder(String s) {
        // constructor that creates object initialized with string in StringBuilder object
        this.doAppend(s);
    }

    public void doAppend(String s) {
        // calls append method from StringBuilder class
        event.append(s);
    }

    public void doInsert(int o, String s) {
        // calls insert method from StringBuilder class
        event.insert(o, s);
    }

    public int getCapacity() {
        // finds capacity of StringBuilder in memory
        return event.capacity();
    }

    public String toString() {
        // returns string form of StringBuilder
        return event.toString();
    }

}