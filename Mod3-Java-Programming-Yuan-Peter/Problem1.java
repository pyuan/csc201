import java.util.Scanner;

public class Problem1 {
    public static void main(String[] args) {
        // create locker objects
        Locker locker100 = new Locker(100, "Mickey Mouse", 3, 28, 17, 39); // enter 28 11 22 to open
        Locker locker275 = new Locker(275, "Donald Duck", 0, 35, 16, 27); // enter 35 19 11 to open

        // open locker100
        locker100.openLocker();

        // put 1 book in locker
        locker100.putBookInLocker();

        // open locker275
        locker275.openLocker();

        // remove 1 book from locker275
        locker275.removeBookFromLocker();

        // print both locker states
        System.out.println("Locker 100's state is:\n" + locker100.toString());
        System.out.println("Locker 275's state is:\n" + locker275.toString());
    }
}

class CombinationLock {
    private int[] combination = new int[3];
    private int dialPosition = 0;

    CombinationLock() {
        // default constructor
    }

    CombinationLock(int a, int b, int c) {
        // set combination
        combination[0] = a;
        combination[1] = b;
        combination[2] = c;
    }

    public int turnLeft(int tick) {
        // if dialposition has to go past 0 tick mark, account for dial 0-39
        // example: if dial posisiton is 1 and the next tick mark is 39 
        // then this will return correct dial position based on user input of ticks turned
        if (dialPosition - tick < 0) {
            int extra = (dialPosition - tick);
            dialPosition = (40 + extra);
        }
        else {
            dialPosition -= tick;
        }
        return dialPosition;
    }

    public int turnRight(int tick) {
        // same as turnLeft accept accounts for sums greater than 39
        // example: dialposisiton is 30 and must turn to 10
        // this will return correct dial position based on user input of ticks turned
        if (dialPosition + tick > 39) {
            int extra = (dialPosition + tick) - 39;
            dialPosition = extra - 1;
        }
        else {
            dialPosition += tick;
        }
        return dialPosition;
    }

    public boolean openLock(int a, int b, int c) {
        // process of how to open the lock
        // takes arguments to make another array
        int[] combinationTry = {turnRight(a), turnLeft(b), turnRight(c)};
        // checks if every same index between arrays is the same 
        if (combinationTry[0] == combination[0] && combinationTry[1] == combination[1] && combinationTry[2] == combination[2]) {
            return true;
        }
        else {
            return false;
        }
    }

    public void resetDial() {
        // resets dial position to 0
        dialPosition = 0;
    }

}

class Locker {
    private int lockerNumber;
    private int numberOfBooks = 0;
    private String studentName;
    private boolean lockerOpen = false;
    CombinationLock lockCombo = new CombinationLock();

    public String toString() {
        //string representation of object
        return "The locker number is " + getLockerNumber() + "\nThe student's name is " + getStudentName() + "\nThere are " + getNumberOfBook() + " books in this locker";
    }

    public int getLockerNumber() {
        return lockerNumber;
    }

    public int getNumberOfBook() {
        return numberOfBooks;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setLockerNumber(int input) {
        lockerNumber = input;
    }

    public void setStudentName(String input) {
        studentName = input;
    }

    public void setNumberOfBooks(int input) {
        numberOfBooks = input;
    }

    Locker() {
        // no arg constructor
        lockerNumber = 0;
        studentName = "John Smith";
    }

    Locker(int lockerNumber, String studentName, int numberOfBooks, int a, int b, int c) {
        // sets object attributes to arguments
        setLockerNumber(lockerNumber);
        setStudentName(studentName);
        setNumberOfBooks(numberOfBooks);
        // creates CombinaitonLock object
        this.lockCombo = new CombinationLock(a, b, c);
    }

    void putBookInLocker() {
        // checks if locker is open
        if (lockerOpen == true) {
            System.out.println("Added one book to the locker");
            // adds 1 book
            numberOfBooks++;
        }
        else {
            System.out.println("Locker is not open");
        }
    }
    
    public boolean removeBookFromLocker() {
        // checks if locker is open and if there are more than 0 books
        if (lockerOpen == true && numberOfBooks > 0) {
            // subtracts 1 book
            numberOfBooks--;
            return true;
        }
        else {
            System.out.println("There are no books or the locker is closed!\n");
            return false;
        }
    }

    public void openLocker() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the ticks turned to represent the combination of your lock");
        // accepts user input for combination ticks
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        // checks if locker is open
        if (lockCombo.openLock(a, b, c) == true) {
            lockerOpen = true;
            System.out.println("Combination is correct, Locker is open");
        }
        else {
            lockerOpen = false;
            System.out.println("Combination is wrong, locker is closed");
        }
    }
}