public class Problem5 {
    public static void main(String[] args) {

        // creates CharacterArray object initialized with address
        CharacterArray array1 = new CharacterArray("NVCC Manassas Campus 6901 Sudley Road Manassas VA 20109");

        // prints input without changes
        System.out.println("The original string is:\n" + array1.originalChar());

        // prints L for letter or D for digit
        System.out.println("The following shows whether the character is a letter(L) or digit(D)\n" + array1.digitOrString());

        // calls redisplay method that swaps letter case and dgiits are '*'
        System.out.println("This redisplays upper and lower cases swapped and digits are '*'\n" + array1.redisplay());
    }
}

class CharacterArray {
    char[] storage;
    String board = "";
    String redo = "";

    CharacterArray() {
        // no arg constructor
    }

    CharacterArray(String s) {
        // makes array the size of the input stirng
        this.storage = new char[s.length()];
        
        // inputs letters from string as array of chars
        for (int i = 0; i < s.length(); i++) {
            storage[i] = s.charAt(i);
        }
    }

    public String originalChar() {
        // returns a string based on char array
        return new String(storage);
    }

    public String digitOrString() {
        // loops over every char in array
        for (int i = 0; i < this.storage.length; i++) {
            // conditional to see if char is a letter or digit and then printing corresponding letter
            if (Character.isLetter(this.storage[i]) == true) {
                this.board += "L ";
            }
            else if (Character.isDigit(this.storage[i]) == true) {
                this.board += "D ";
            }
        } 
        return board;
    }

    public String redisplay() {
        // loops over every char in array
        for (int i = 0; i < this.storage.length; i++) {
            // if uppercase then change to lowercase
            if (Character.isUpperCase(this.storage[i]) == true) {
                redo += Character.toLowerCase(this.storage[i]);
            }
            // if lowercaser then change to uppercase
            else if (Character.isLowerCase(this.storage[i]) == true) {
                redo += Character.toUpperCase(this.storage[i]);
            }
            // if digit then replace with '*'
            else if (Character.isDigit(this.storage[i]) == true) {
                redo += "*";
            }
            // if space keep it as a space
            else {
                redo += " ";
            }
        }
        return redo;
    }
}