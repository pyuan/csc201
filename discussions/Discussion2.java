
import java.util.Scanner;

public class Discussion2 {
    public static void main(String[] args) {
        System.out.println("Enter a 4-by-4 matrix row by row:");
        double[][] matrix = new double[4][4];
        Scanner input = new Scanner(System.in);
        System.out.println("Enter " + matrix.length + " rows and " 
        + matrix[0].length + " columns: ");
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                matrix[row][column] = input.nextDouble();
            }
        }
        input.close();
        System.out.println("Average of the elements in the major diagonal is " + averageMajorDiagonal(matrix));
    }
    
    public static double averageMajorDiagonal(double[][] m) {
        double total = 0;
        for (int i = 0; i < m.length; i++) {
            total += m[i][i];
        }
        return total / 4.0;

        //return (((m[0][0] + m[1][1] + m[2][2] + m[3][3]) / 4.0));
    }
}
