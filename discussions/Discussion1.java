import java.util.Scanner;

public class Discussion1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your string");
        String userInput = input.nextLine();
        input.close();
        System.out.println("Your string has " + countLetters(userInput) + " letters");
    }

    public static int countLetters(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); ++i) {
            char charFromString = s.charAt(i);
            if (Character.isLetter(charFromString) == true) {
            ++count;
            }
        }
        return count;
    }
}
