public class LoanExample {
    public static void main(String[] args) {
        try {
            // constructor parameters: annual interest rate, number of years, loan amount
            //                         double              ,  int           , double
            Loan loan1 = new Loan(5, 20, 40000);
            System.out.println("First Loan:" +
            "\nMontly Payment: " + loan1.getMonthlyPayment() +
            "\nTotal Payment: " + loan1.getTotalPayment());
        }
        catch (IllegalArgumentException ex) {
            // prints out error and what value cannot be less than or equal to 0
            System.out.println(ex.toString());
        }

        try {
            System.out.println("\nSecond Loan");
            Loan loan2 = new Loan();
            loan2.setAnnualInterestRate(10);
            loan2.setLoanAmount(2000);
            loan2.setNumberOfYears(-10);
            System.out.println("Montly Payment: " + loan2.getMonthlyPayment() +
            "\nTotal Payment: " + loan2.getTotalPayment());
        }
        catch (IllegalArgumentException ex) {
            // prints out error and what value cannot be less than or equal to 0
            System.out.println(ex.toString());
        }
    }
}