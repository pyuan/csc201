public class Discussion4 {
    public static void main(String[] args) {
        Course csc201 = new Course("csc201");
        csc201.addStudent("Peter");
        csc201.addStudent("Joe");
        csc201.addStudent("Jessica");
        System.out.println("The couse in this example is " + csc201.getCourseName());
        System.out.println("There are " + csc201.getNumberOfStudents() + " students in csc201");
        System.out.println("The students enrolled in csc201 are " + csc201.getStudents());
        csc201.dropStudent("Joe");
        System.out.println("However, after week 1 Joe decided to drop out");
        System.out.println("Now there are " + csc201.getNumberOfStudents() + " students in csc201");
        System.out.println("They are " + csc201.getStudents());
    }
}