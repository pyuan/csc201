public class Discussion3 {
    public static void main(String[] args) {
    MyPoint p1 = new MyPoint();    
    MyPoint p2 = new MyPoint(10, 30.5);
    System.out.println("The distance between p1 and p2 is " + MyPoint.distance(p1, p2));
    }
}

class MyPoint {
    private double x, y;

    MyPoint() {
        x = 0;
        y = 0;
    }

    MyPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(MyPoint p1) {
        return Math.sqrt( Math.pow((this.x - p1.getX()), 2) + Math.pow((this.y - p1.getY()), 2) );
    }

    public double distance(double x, double y) {
        return Math.sqrt( Math.pow((this.x - x), 2) + Math.pow((this.y - y), 2) );
    }

    public static double distance(MyPoint p1, MyPoint p2) {
        return Math.sqrt( Math.pow((p1.getX() - p2.getX()), 2) + Math.pow((p1.getY()- p2.getY()), 2) );
    }
}