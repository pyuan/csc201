import java.util.ArrayList;

public class Course {
    private String courseName;
    //private String[] students = new String[100];
    private ArrayList<String> students = new ArrayList<>();
    //private int numberOfStudents; 
    // There is no more need for numberOfStudents because arraylist can return the number of students

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(String student) {
        //students[numberOfStudents] = student;
        students.add(student);
        //numberOfStudents++; 
        // array list replace the need for numberOfStudents
    }
    public String getStudents() {
        //return students;
        return students.toString();
    } 

    public int getNumberOfStudents() {
        //return numberOfStudents;
        // this returns the numberOfStudents without an extra variable
        return students.size();
    } 

    public String getCourseName() {
        return courseName;
    }

    public void dropStudent(String student) {
        // bonus answer to exercise 10.9
        students.remove(student);
    }
}