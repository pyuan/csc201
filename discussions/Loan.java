public class Loan {
    private double annualInterestRate;
    private int numberOfYears;
    private double loanAmount;
    private java.util.Date loanDate;        

    public Loan() {
        this(2.5, 1, 1000);
    }
    public Loan(double annualInterestRate, int numberOfYears,
    double loanAmount) {
        // throws IllegalArgumentException is any value is less than or equal to 0
        // exception is handeled in main
        if (annualInterestRate <= 0) {
            throw new IllegalArgumentException("Annual interest rate cannot be less than or equal to 0");
        }
        else if (numberOfYears <= 0) {
            throw new IllegalArgumentException("Number of years cannot be less than or equal to 0");
        }
        else if (loanAmount <= 0) {
            throw new IllegalArgumentException("Loan Amount cannot be less than or equal to 0");
        }
        else {
            this.annualInterestRate = annualInterestRate;
            this.numberOfYears = numberOfYears;
            this.loanAmount = loanAmount;
            loanDate = new java.util.Date();
        }
    }

    /** Return annualInterestRate */
    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /** Set a new annualInterestRate */
    public void setAnnualInterestRate(double annualInterestRate) {
        if (annualInterestRate <= 0) {
            throw new IllegalArgumentException("Annual Interest Rate cannot be less than or equal to 0");
        }
        else {
            this.annualInterestRate = annualInterestRate;
        }
    }

    /** Return numberOfYears */
    public int getNumberOfYears() {
        return numberOfYears;
    }

    /** Set a new numberOfYears */
    public void setNumberOfYears(int numberOfYears) {
        if (numberOfYears <= 0) {
            throw new IllegalArgumentException("Number of years cannot be less than or equal to 0");
        }
        else {
            this.numberOfYears = numberOfYears;
        }
    }

    /** Return loanAmount */
    public double getLoanAmount() {
        return loanAmount;
    }

    /** Set a new loanAmount */
    public void setLoanAmount(double loanAmount) {
        if (loanAmount <= 0) {
            throw new IllegalArgumentException("Loan Amount cannot be less than or equal to 0");
        }
        else {
            this.loanAmount = loanAmount;
        }
    }

    /** Find monthly payment */
    public double getMonthlyPayment() {
        double monthlyInterestRate = annualInterestRate / 1200;
        double monthlyPayment = loanAmount * monthlyInterestRate / (1 - (1 / Math.pow(1 + monthlyInterestRate, numberOfYears * 12)));
        return monthlyPayment;
    }

    /** Find total payment */
    public double getTotalPayment() {
        double totalPayment = getMonthlyPayment() * numberOfYears * 12;
        return totalPayment;
    }

    /** Return loan date */
    public java.util.Date getLoanDate() {
        return loanDate;
    } 
}