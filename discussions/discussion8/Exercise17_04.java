import java.io.IOException;
import java.util.Scanner;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.File;

public class Exercise17_04 {
    public static void main(String[] args) throws IOException {
        try (
            Scanner input = new Scanner(new File("Welcome.java"));
            DataOutputStream output = new DataOutputStream(new FileOutputStream("Welcome.utf"));
        ) {
            while (input.hasNext()) {
                output.writeUTF(input.nextLine());
            }
        }
        System.out.println("Welcome.java size: " + findFileSize("Welcome.java") + " bytes\n" + 
        "Welcome.utf size: " + findFileSize("Welcome.utf") + " bytes");
        
    }

    public static long findFileSize(String filename) {
        File file = new File(filename);
        return file.length();
    }
}